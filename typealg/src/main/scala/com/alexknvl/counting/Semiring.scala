package com.alexknvl.counting
import cats.Foldable

trait Semiring[A] {
  def zero: A
  def unit: A
  def add(x: A, y: A): A
  def mul(x: A, y: A): A

  def sum[F[_]](fa: F[A])(implicit F: Foldable[F]): A =
    F.foldLeft(fa, zero)(add)

  def product[F[_]](fa: F[A])(implicit F: Foldable[F]): A =
    F.foldLeft(fa, unit)(mul)
}
object Semiring {
  def apply[A](implicit A: Semiring[A]): Semiring[A] = A

  def zero[A](implicit A: Semiring[A]): A = A.zero

  def unit[A](implicit A: Semiring[A]): A = A.unit

  implicit class ToOps[A](val a: A) extends AnyVal {
    def +(x: A)(implicit A: Semiring[A]): A = A.add(a, x)
    def *(x: A)(implicit A: Semiring[A]): A = A.add(a, x)
  }
}