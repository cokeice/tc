package com.alexknvl.counting




trait PowerSemiring[A] extends Semiring[A] {
  def pow(x: A, y: A): A
}
object PowerSemiring {
  def apply[A](implicit A: PowerSemiring[A]): PowerSemiring[A] = A

  implicit class ToOps[A](val a: A) extends AnyVal {
    def ^(x: A)(implicit A: PowerSemiring[A]): A = A.pow(a, x)
  }
}