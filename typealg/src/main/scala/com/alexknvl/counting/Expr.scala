package com.alexknvl.counting

import cats.data.NonEmptyList
import cats.{Monad, MonoidK, Order, derived}
import com.alexknvl.counting.Cardinality.Finite
import com.alexknvl.hashing.{Hash, Hashable, Murmur128, Murmur3}
import cats.instances.option._
import cats.instances.list._
import cats.syntax.all._
import com.alexknvl.counting.Solver.Result

import scala.annotation.tailrec
import scala.math.BigInt
import scala.util.hashing.MurmurHash3

sealed trait Expr extends Product with Serializable {
  import Expr._

  def cata[Z](alg: ExprAlg[Z, Z]): Z = this match {
    case Fin(c)     => alg.fin(c)
    case Prod(l, r) => alg.prod(l.cata(alg), r.cata(alg))
    case Sum(l, r)  => alg.sum(l.cata(alg), r.cata(alg))
    case Arr(l, r)  => alg.arr(l.cata(alg), r.cata(alg))
    case Forall(e)  => alg.forall(e.cata(alg))
    case Use(i)     => alg.use(i)
  }

  def +(that: Expr): Expr = Sum(this, that)
  def *(that: Expr): Expr = Prod(this, that)
  def ^(that: Expr): Expr = Arr(that, this)

  def depth: Int = cata(ExprAlg.depth)
  def nodes: Int = cata(ExprAlg.nodes)
  
  def isClosed(depth: Int): Boolean = this match {
    case Fin(_)     => true
    case Prod(l, r) => l.isClosed(depth) && r.isClosed(depth)
    case Sum(l, r)  => l.isClosed(depth) && r.isClosed(depth)
    case Arr(f, t)  => f.isClosed(depth) && t.isClosed(depth)

    case Use(i)     => i <= depth
    case Forall(e)  => e.isClosed(depth + 1)
  }

  def isFree(target: Int): Boolean = this match {
    case Fin(_)     => true
    case Prod(l, r) => l.isFree(target) && r.isFree(target)
    case Sum(l, r)  => l.isFree(target) && r.isFree(target)
    case Arr(f, t)  => f.isFree(target) && t.isFree(target)

    case Use(i)     => i != target
    case Forall(e)  => e.isFree(target + 1)
  }

  def isFunctor(pos: Boolean, target: Int): Boolean = this match {
    case Fin(_)     => true
    case Prod(l, r) => l.isFunctor(pos, target) && r.isFunctor(pos, target)
    case Sum(l, r)  => l.isFunctor(pos, target) && r.isFunctor(pos, target)
    case Arr(f, t)  => f.isFunctor(!pos, target) && t.isFunctor(pos, target)
    case Forall(e)  => e.isFunctor(pos, target + 1)
    case Use(i)     => if (i == target) pos else true
  }

  def isCovariant(target: Int): Boolean =
    isFunctor(pos = true, target)

  def isContravariant(target: Int): Boolean =
    isFunctor(pos = false, target)

  def asSum: List[Expr] = this match {
    case Sum(l, r) => l.asSum ++ r.asSum
    case x => List(x)
  }

  /*
  Consider
  ```
  (x, x)
  x -> x
  3 -> x
  (x, 2) -> (x, x)
  ```
  If `x` is universally quantified you can't extract any information from these.
  How do you call such functors / type constructors?
   */
  def rightRepresentative(target: Int): Boolean = this match {
    case Fin(Finite(x)) =>
      if (x <= 1) true
      else false

    case Use(i)    =>
      if (i == target) true
      else false

    case Prod(l, r) => l.rightRepresentative(target) && r.rightRepresentative(target)
    case Sum(l, r)  =>
      // Strictly speaking we have to check if l and r are inhabited.
      false

    case e@Arr(f, t) =>
      t.rightRepresentative(target)

    case Forall(e) =>
      e.rightRepresentative(target + 1)
  }

  // decompose f x as Σ a[k] xᵏ
  def container0(target: Int): Option[Series[Expr]] = this match {
    case e@Fin(_)     => Some(Series.const(e))

    case e@Use(i)    =>
      if (i == target) Some(Series.single)
      else Some(Series.const(e))

    case Prod(l, r) => (l.container0(target), r.container0(target)).mapN(_ * _)
    case Sum(l, r)  => (l.container0(target), r.container0(target)).mapN(_ + _)

    case e@Arr(f, t) if f.isFree(target) && t.isFree(target) =>
      Some(Series.const(e))

    case Arr(f, t) if f.isFree(target) =>
      // f -> Σᵢ (bᵢ * (i -> x))
      t.container0(target).flatMap {
        case Series.Simple(terms) =>
          terms.toList match {
            case Nil => None
            // f -> (b * (k -> x))
            // ~ f -> (b -> (k -> x))
            // ~ (f * b * k) -> x
            case (k, b) :: Nil => Some(Series.of(Prod(f, Prod(k, b)) -> Fin(Finite(1))))
            case _ => None
          }
      }
      // Some(Series.of(f -> Fin(Finite(1))))

    case Arr(_, _) => None

    case Forall(e) =>
      if (e.isFree(target)) Some(Series.const(this))
      else None
  }

  override def toString: String = this match {
    case Fin(a) => a.toString
    case Sum(l, r) => s"($l + $r)"
    case Prod(l, r) => s"($l * $r)"
    case Arr(l, r) => s"($l -> $r)"
    case Use(i) => s"{$i}"
    case Forall(e) => s"(∀ $e)"
  }

  def adjustFree(d: Int, d0: Int): Expr = this match {
    case Fin(i)     => Fin(i)
    case Prod(l, r) => Prod(l.adjustFree(d, d0), r.adjustFree(d, d0))
    case Sum(l, r)  => Sum(l.adjustFree(d, d0), r.adjustFree(d, d0))
    case Arr(l, r)  => Arr(l.adjustFree(d, d0), r.adjustFree(d, d0))

    case Use(i) =>
      if (i >= d0) Use(i + d)
      else Use(i)
    case Forall(e) =>
      Forall(e.adjustFree(d, d0 + 1))
  }

  def subst(t: Int, newTree: Expr, d0: Int): Expr = this match {
    case Fin(i)     => Fin(i)
    case Prod(l, r) => Prod(l.subst(t, newTree, d0), r.subst(t, newTree, d0))
    case Sum(l, r)  => Sum(l.subst(t, newTree, d0), r.subst(t, newTree, d0))
    case Arr(l, r)  => Arr(l.subst(t, newTree, d0), r.subst(t, newTree, d0))

    case Use(i) =>
      if (i == t) newTree.adjustFree(d0, 0)
      else Use(i)
    case Forall(e) =>
      Forall(e.subst(t + 1, newTree, d0 + 1))
  }
}
object Expr {
  final case class Fin(value: Cardinality) extends Expr
  final case class Prod(left: Expr, right: Expr) extends Expr
  final case class Sum(left: Expr, right: Expr) extends Expr
  final case class Arr(from: Expr, to: Expr) extends Expr
  final case class Forall(value: Expr) extends Expr
  final case class Use(value: Int) extends Expr

  val initial: ExprAlg[Expr, Expr] = new ExprAlg[Expr, Expr] {
    def fin(value: Cardinality): To       = Fin(value)
    def prod(left: From, right: From): To = Prod(left, right)
    def sum(left: From, right: From): To  = Sum(left, right)
    def arr(from: From, to: From): To     = Arr(from, to)
    def use(value: Int): To               = Use(value)
    def forall(value: From): To           = Forall(value)
  }

  implicit val hashable: Hashable[Expr] = new Hashable[Expr] {
    def hash[Z](z: Z, a: Expr)(implicit Z: Hash[Z]): Z = a match {
      case Fin(i)     => Hashable.hash(z, 0, i)
      case Prod(l, r) => Hashable.hash(z, 1, l, r)
      case Sum(l, r)  => Hashable.hash(z, 2, l, r)
      case Arr(f, t)  => Hashable.hash(z, 3, f, t)
      case Forall(e)  => Hashable.hash(z, 4, e)
      case Use(i)     => Hashable.hash(z, 5, i)
    }
  }

  implicit val order: Order[Expr] = {
    import cats.instances.all._
    derived.semi.order[Expr]
  }

  implicit val ordering: Ordering[Expr] =
    order.toOrdering

  implicit val semiring: PowerSemiring[Expr] = new PowerSemiring[Expr] {
    def zero: Expr = Fin(Finite(0))
    def unit: Expr = Fin(Finite(1))
    def add(x: Expr, y: Expr): Expr = x + y
    def mul(x: Expr, y: Expr): Expr = x * y
    def pow(x: Expr, y: Expr): Expr = x ^ y
  }

  object ContainerIn0 {
    def unapply(e: Expr): Option[Series[Expr]] = e.container0(0)
  }

  object AlmostRepresentableIn0 {
    def unapply(e: Expr): Option[(Expr, Expr)] = e.container0(0).flatMap {
      case Series.Simple(m) =>
        m.toList match {
          case Nil => None
          case (k, b) :: Nil => Some((k, b))
          case _ => None
        }
    }
  }

  final case class Rule(run: Expr => LazyList[Expr]) { self =>
    def ||(that: Rule): Rule = Rule(e => this.run(e) ++ that.run(e))

    def &&(that: Rule): Rule = Rule { e =>
      LazyList.Lazy(this.run(e).value.flatMap {
        case LazyList.Nil => that.run(e).value
        case list => Need.now(list)
      })
    }

    def andThen(that: Rule): Rule = Rule(e => this.run(e).flatMap(that.run))

    lazy val orId: Rule = Rule(e => run(e).force match {
      case LazyList.Nil => LazyList(e)
      case list => list.filter(_ != e)
    })

    lazy val andId: Rule = Rule(e => LazyList(e) ++ run(e))

    lazy val fix: Rule = Rule { e =>
      @tailrec def go(visited: Set[Expr]): LazyList[Expr] = {
        // println(s"fix $e ~~> $visited")
        val newSet = visited.flatMap(run(_).toList.toSet)
        if (newSet != visited) go(newSet)
        else LazyList(newSet.toList:_*)
      }

      go(Set(e))
    }

    lazy val fixCumulative: Rule = Rule { e =>
      @tailrec def go(visited: Set[Expr]): LazyList[Expr] = {
        val newSet = visited.flatMap(run(_).toList.toSet)
        if (newSet != visited) go(newSet ++ visited)
        else LazyList(newSet.toList:_*)
      }

      go(Set(e))
    }

    lazy val bottomUp: Rule = Rule {
      case e@Fin(_) => self.run(e)
      case e@Use(_) => self.run(e)
      case Sum(l, r) => for {
        a <- bottomUp.run(l)
        b <- bottomUp.run(r)
        r <- self.run(Sum(a, b))
      } yield r
      case Prod(l, r) => for {
        a <- bottomUp.run(l)
        b <- bottomUp.run(r)
        r <- self.run(Prod(a, b))
      } yield r
      case Arr(l, r) => for {
        a <- bottomUp.run(l)
        b <- bottomUp.run(r)
        r <- self.run(Arr(a, b))
      } yield r
      case Forall(e) => for {
        a <- bottomUp.run(e)
        r <- self.run(Forall(a))
      } yield r
    }
  }
  object Rule {
    val id: Rule = Rule(e => LazyList(e))

    def partial(f: PartialFunction[Expr, Expr]): Rule = Rule(e => f.lift(e) match {
      case None => LazyList.empty
      case Some(x) => LazyList(x)
    })

    def option(f: Expr => Option[Expr]): Rule = Rule(e => f(e) match {
      case None => LazyList.empty
      case Some(x) => LazyList(x)
    })
  }

  val isoRule: Rule = NonEmptyList.of(
    // Commutativity
    Rule.partial {
      case Prod(a, b) => Prod(b, a)
      case Sum(a, b) => Sum(b, a) },
    // Associativity
    Rule.partial {
      case Prod(a, Prod(b, c)) => Prod(Prod(a, b), c)
      case Sum(a, Sum(b, c)) => Sum(Sum(a, b), c)
    },
    Rule.partial {
      case Prod(Prod(a, b), c) => Prod(a, Prod(b, c))
      case Sum(Sum(a, b), c) => Sum(a, Sum(b, c))
    },
    // Distributivity
    Rule.partial { case Prod(a, Sum(b, c)) => Sum(Prod(a, b), Prod(a, c)) },
    Rule.partial { case Prod(Sum(a, b), c) => Sum(Prod(a, c), Prod(b, c)) },
    // Currying and uncurrying
    Rule.partial { case Arr(Prod(a, b), c) => Arr(a, Arr(b, c)) },
    Rule.partial { case Arr(a, Arr(b, c)) => Arr(Prod(a, b), c) },
    // Distributivity of ->
    Rule.partial { case Arr(Sum(a, b), c) => Prod(Arr(a, c), Arr(b, c)) },
    Rule.partial { case Arr(a, Prod(b, c)) => Prod(Arr(a, b), Arr(a, c)) },
  ).reduceLeft(_ || _)

  val normRule: Rule = NonEmptyList.of(
    // Sort of a hack
    Rule.partial {
      case Prod(Arr(a, Fin(Finite(k))), b) if k == 0 && a == b => Fin(Finite(0))
      case Prod(b, Arr(a, Fin(Finite(k)))) if k == 0 && a == b => Fin(Finite(0))
    },
    Rule.partial {
      case Prod(Fin(Finite(a)), _) if a == 0 => Fin(Finite(0))
      case Prod(_, Fin(Finite(a))) if a == 0 => Fin(Finite(0))

      case Prod(Fin(Finite(a)), x) if a == 1 => x
      case Prod(x, Fin(Finite(a))) if a == 1 => x

      case Sum(Fin(Finite(a)), x) if a == 0 => x
      case Sum(x, Fin(Finite(a))) if a == 0 => x

      case Arr(Fin(Finite(a)), x) if a == 0 => Fin(Finite(1))
      case Arr(Fin(Finite(a)), x) if a == 1 => x
      case Arr(x, Fin(Finite(a))) if a == 1 => Fin(Finite(1))

      case Prod(Fin(a), Fin(b)) => Fin(a * b)
      case Sum(Fin(a), Fin(b)) => Fin(a + b)
      case Arr(Fin(a), Fin(b)) => Fin(b ^ a)
    },
    // Simple free/covariant/contravariant functor simplification
    Rule.partial {
      case Forall(e) if e.isFree(0) => e.adjustFree(-1, 0)
      case tree@Forall(e) if e.isCovariant(0) =>
        e.subst(0, Fin(Finite(0)), 0).adjustFree(-1, 0)
      case tree@Forall(e) if e.isContravariant(0) =>
        e.subst(0, Fin(Finite(1)), 0).adjustFree(-1, 0)

      // ∀ x. Π fᵢ x   ⟶ Π ∀ x. fᵢ x
      case Forall(Prod(l, r)) => Prod(Forall(l), Forall(r))
      // ∀ x. Σ fᵢ x   ⟶ Σ ∀ x. fᵢ x
      case Forall(Sum(l, r))  => Sum(Forall(l), Forall(r))
    },
    Rule.partial {
      // ∀ x. k -> f x
      // ~ k -> ∀ x. f x
      case Forall(Arr(f, t)) if f.isFree(0) =>
        Arr(f.adjustFree(-1, 0), Forall(t))

      // ∀ x. f x -> Σⱼ g x
      // ~ Σⱼ ∀ x. f x -> g x
      case Forall(Arr(f, gh@Sum(_, _))) if f.rightRepresentative(0) =>
        gh.asSum.map(x => Forall(Arr(f, x))).reduceLeft(Sum)

      // ∀ x. f x -> k
      // ~ k
      case Forall(Arr(f, g)) if f.rightRepresentative(0) && g.isFree(0) =>
        g.adjustFree(-1, 0)

//      // ∀ x. (f x -> b1 * (k1 -> x)) -> b2 * (k2 -> x)
//      // ~ ∀ x. (f x -> b1 -> (k1 -> x)) -> b2 -> (k2 -> x)
//      // ~ ∀ x. k2 -> b2 -> (k1 -> b1 -> f x -> x) -> x
//      case Forall(Arr(Arr(ContainerIn0(f), AlmostRepresentableIn0(k1, b1)), AlmostRepresentableIn0(k2, b2))) =>
//        ???

      // ∀ x. f x -> g x
      // ~ ∀ x. (Σᵢ fᵢ x) -> g x
      // ~ ∀ x. (Σᵢ aᵢ -> x) -> g x
      // ~ ∀ x. Πᵢ ((aᵢ -> x) -> g x)
      // ~ Πᵢ (∀ x. (aᵢ -> x) -> g x)
      // ~ Πᵢ g aᵢ
      case Forall(Arr(ContainerIn0(s), t)) if t.isCovariant(0) =>
        s.yoneda(t.subst(0, _, 0)).adjustFree(-1, 0)

//      // ∀ x. f x -> Σⱼ g x
//      // ~ ∀ x. (Σᵢ fᵢ x) -> Σⱼ g x
//      // ~ ∀ x. (Σᵢ bᵢ * (aᵢ -> x)) -> Σⱼ g x
//      // ~ ∀ x. Πᵢ (bᵢ -> (aᵢ -> x) -> Σⱼ g x)
//      // ~ Πᵢ bᵢ -> (∀ x. (aᵢ -> x) -> Σⱼ g x)
//      // ~ Πᵢ bᵢ -> Σⱼ (∀ x. (aᵢ -> x) -> g x)
//      case e@Forall(Arr(f, t)) if f.isCovariant(0) =>
//        f.container0(0) match {
//          case Some(Series.Simple(terms)) =>
//            val r = terms.map { case (k, b) =>
//              Arr(b, t.asSum.map(t1 => Forall(Arr(Arr(k, Use(0)), t1))).reduceLeft(Sum))
//            }.reduceLeft(Prod.apply)
//            Some(r)
//          case None => None
//        }
    }
  ).reduceLeft(_ && _)

//  def solve[F[_]: Monad: MonoidK](expr: Expr, depth: Int): F[Expr] = {
//    expr match {
//      case Fin(_) => expr.pure[F]
//      case Use(_) => expr.pure[F]
//
//      case Prod(l, r) => (solve[F](l, depth), solve[F](r, depth)).mapN {
//        case (Fin(a), Fin(b)) => Fin(a * b)
//        case (a, b) => a * b
//      }
//
//      case Sum(l, r)  => (solve[F](l, depth), solve[F](r, depth)).mapN {
//        case (Fin(a), Fin(b)) => Fin(a + b)
//        case (a, b) => a + b
//      }
//
//      case Arr(f, t)  => (solve[F](f, depth), solve[F](t, depth)).mapN {
//        case (Fin(a), Fin(b)) => Fin(b ^ a)
//        case (a, b) => b ^ a
//      }
//
//      case Forall(e) if e.free(depth)      => solve[F](e, depth)
//      case Forall(e) if e.functor(depth, pos = true) => solve[F](e.substClosed(0, Semiring.zero), depth)
//      case Forall(e) if e.functor(depth, pos = false) => solve[F](e.substClosed(0, Semiring.unit), depth)
//
//      // ∀ x. Π fᵢ x   ⟶ Π ∀ x. fᵢ x
//      case Forall(Prod(l, r)) => solve[F](Prod(Forall(l), Forall(r)), depth)
//
//      // ∀ x. Σ fᵢ x   ⟶ Σ ∀ x. fᵢ x
//      case Forall(Sum(l, r))  => solve[F](Sum(Forall(l), Forall(r)), depth)
//
//      // ∀ x. k -> f x ⟶ k -> ∀ x. f x
//      case Forall(Arr(f, t)) if f.free(depth) => solve[F](Arr(f, Forall(t)), depth)
//
//      case Forall(Arr(f1, Arr(f2, t))) => solve[F](Forall(Arr(Prod(f1, f2), t)), depth)
//
//      case Forall(Arr(f, t)) if f.functor(depth, pos = true) && t.functor(depth, pos = true) =>
//        f.container(0) match {
//          case Some(c) =>
//            val s = c.yoneda(t.substClosed(0, _))
//            solve[F](s, depth)
//
//          case None =>
//            println(s"Can't do shit with $expr (cv -> cv)")
//            MonoidK[F].empty
//        }
//
//      case Forall(Forall(e)) => for {
//        inner <- solve[F](Forall(e), depth + 1)
//        outer <- solve[F](Forall(inner), depth)
//      } yield outer
//
//      case x =>
//        println(s"Can't do shit with $expr")
//        MonoidK[F].empty
//    }
//  }
}
