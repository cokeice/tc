package com.alexknvl.counting

import org.scalacheck.{Gen, Shrink}
import org.scalatest.PropSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import cats.instances.list._
import com.alexknvl.counting.Solver.Result
import org.scalactic.Prettifier
import org.scalatest
import org.scalatest.enablers.CheckerAsserting

import scala.Predef.{any2stringadd => _, _}

object Gens {
  /*
  final case class Fin(value: Cardinality) extends Expr
  final case class Prod(list: List[Expr]) extends Expr
  final case class Sum(list: List[Expr]) extends Expr
  final case class Arr(from: Expr, to: Expr) extends Expr
  final case class Forall(value: Expr) extends Expr
  final case class Use(value: Int) extends Expr
   */

  def cardinality: Gen[Cardinality] =
    Gen.frequency(
      5 -> Gen.choose(0, 3).map(Cardinality.Finite(_)),
      1 -> Gen.const(Cardinality.Infinite))

  def expr(vars: Int, size: Int): Gen[Expr] = {
    val fin = List(cardinality.map(Expr.Fin))

    val nested =
      if (size >= 2) List(
        for {
          a <- Gen.delay(expr(vars, size / 2))
          b <- expr(vars, size / 2)
        } yield a * b,
        for {
          a <- Gen.delay(expr(vars, size / 2))
          b <- expr(vars, size / 2)
        } yield a + b,
        for {
          a <- Gen.delay(expr(vars, size / 2))
          b <- expr(vars, size / 2)
        } yield a ^ b,
        Gen.delay(expr(vars + 1, size - 1)).map(Expr.Forall))
      else Nil

    val use =
      if (vars >= 1) List(Gen.choose(0, vars - 1).map(Expr.Use))
      else Nil

    fin ++ nested ++ use match {
      case Nil => sys.error("impossible")
      case x :: Nil => x
      case x :: y :: xs => Gen.oneOf(x, y, xs:_*)
    }
  }

  implicit val shrinkExpr: Shrink[Expr] = {
    import Expr._
    import Cardinality._
    def minify(e: Expr): LazyList[Expr] = e match {
      case Fin(Infinite)  => LazyList(Fin(Finite(1)))
      case Fin(Finite(a)) => LazyList(Fin(Finite(a)), Fin(Finite(0)))
      case Use(i)         => LazyList(Use(i), Use(i / 2), Use(0))
      case Prod(a, b) =>
        for {
          x <- minify(a)
          y <- minify(b)
          r <- LazyList(x, y, Prod(x, y))
        } yield r
      case Sum(a, b) =>
        for {
          x <- minify(a)
          y <- minify(b)
          r <- LazyList(x, y, Sum(x, y))
        } yield r
      case Arr(a, b) =>
        for {
          x <- minify(a)
          y <- minify(b)
          r <- LazyList(x, y, Arr(x, y))
        } yield r
      case Forall(a) =>
        for {
          x <- minify(a)
          r <- LazyList(Forall(x))
        } yield r
    }

    Shrink[Expr] { e =>
      minify(e).filter(_ != e).distinct.toStream
    }
  }
}

class ExprTests extends PropSpec with GeneratorDrivenPropertyChecks {
  property("parsing") {
    import Expr._
    import Cardinality._

    val p = ExprAlg.parser[Expr](Expr.initial)

    assert(p("10").either.right.get                == Fin(Finite(10)))
    assert(p("1 + 2 * 3").either.right.get         == Sum(Fin(Finite(1)), Prod(Fin(Finite(2)), Fin(Finite(3)))))
    assert(p("(1 + 2) * 3").either.right.get       == Prod(Sum(Fin(Finite(1)), Fin(Finite(2))), Fin(Finite(3))))
    assert(p("1 -> 1").either.right.get            == Arr(Fin(Finite(1)),Fin(Finite(1))))
    assert(p("1 -> 2 -> 3").either.right.get       == Arr(Fin(Finite(1)),Arr(Fin(Finite(2)),Fin(Finite(3)))))
    assert(p("(1 -> 1 + 1) -> 3").either.right.get == Arr(Arr(Fin(Finite(1)),Sum(Fin(Finite(1)), Fin(Finite(1)))),Fin(Finite(3))))
    assert(p("∀ x. 1").either.right.get            == Forall(Fin(Finite(1))))
    assert(p("(∀ x. 1) -> 1").either.right.get     == Arr(Forall(Fin(Finite(1))), Fin(Finite(1))))
    assert(p("1 -> ∀ x. 1").either.right.get       == Arr(Fin(Finite(1)), Forall(Fin(Finite(1)))))
    assert(p("∀ x. x").either.right.get            == Forall(Use(0)))
    assert(p("∀ x y. x -> y").either.right.get     == Forall(Forall(Arr(Use(1),Use(0)))))
    assert(p("∀ x. ∀ y. x -> y").either.right.get  == Forall(Forall(Arr(Use(1),Use(0)))))
    assert(p("∀ x. ∀ x. x -> x").either.right.get  == Forall(Forall(Arr(Use(0),Use(0)))))
    assert(p("∀ y. ∀ x. y -> y").either.right.get  == Forall(Forall(Arr(Use(1),Use(1)))))
    assert(p("∀ x. x -> 0").either.right.get       == Forall(Arr(Use(0),Fin(Finite(0)))))
  }

  property("solver works") {
    val sol1 = Solver.solve[Int, Int](0, 10000, { i: Int =>
      if (i == 12) Solver.Result.done(i)
      else Solver.Result.fork(LazyList(i - 1, i, i + 1))
    })

    assert(sol1 == Some(12))

    val sol2 = Solver.solve[List[(Int, Int)], List[(Int, Int)]](Nil, Int.MaxValue, { i: List[(Int, Int)] =>
      if (i.length == 8) Solver.Result.done(i)
      else {
        val ps = for {
          x <- LazyList.range(0, 8)
          ys  = i.map { case (_, y) => y }
          max = if (ys.nonEmpty) ys.max + 1 else 0
          y <- LazyList.range(0, max + 1)
          xs = i.map { case (x, _) => x }
          ds = i.map { case (x, y) => x - y }
          if !xs.contains(x) && !ys.contains(y) && !ds.contains(x - y)
        } yield (x, y) :: i

        Solver.Result.fork(ps)
      }
    })

    assert(sol2.isDefined)
  }

  property("functor tests") {
    import Expr._
    import Cardinality._

    assert(!Arr(Use(0), Use(0)).isFree(0))
    assert(!Arr(Use(0), Use(0)).isCovariant(0))
    assert(!Arr(Use(0), Use(0)).isContravariant(0))

    assert(!Arr(Use(1), Use(0)).isFree(0))
    assert(Arr(Use(1), Use(0)).isCovariant(0))
    assert(!Arr(Use(1), Use(0)).isContravariant(0))

    assert(!Arr(Use(0), Use(1)).isFree(0))
    assert(!Arr(Use(0), Use(1)).isCovariant(0))
    assert(Arr(Use(0), Use(1)).isContravariant(0))

    assert(Forall(Use(0)).isFree(0))
    assert(Forall(Use(0)).isFree(1))
    assert(!Forall(Use(1)).isFree(0))
    assert(Forall(Forall(Use(1))).isFree(1))
  }

  property("series yoneda") {
    assert(Series.of(0 -> 10, 1 -> 3).yoneda(identity) == 0)
    assert(Series.of(1 -> 2, 1 -> 3).yoneda(identity) == 1)
    assert(Series.of(2 -> 2, 1 -> 3).yoneda(identity) == 4)
    assert(Series.of(2 -> 2, 3 -> 2).yoneda(identity) == 4 * 9)
  }

  property("solve") {
    import Expr._
    import Cardinality._

    val p = {
      val p = ExprAlg.parser[Expr](Expr.initial)
      s: String => p(s).either.right.get
    }

    def small : Expr => Result[Expr, Cardinality] = {
      case Fin(x) => Result.done(x)
      case e =>
        val r = (Expr.normRule.orId.bottomUp.fix andThen Expr.isoRule.andId.bottomUp).run(e).map { e1 =>
          if (!e1.isClosed(0)) sys.error(s"$e ~~> $e1")
          e1
        }
        Result.fork(r)
    }

    def s = (expr: Expr) => Solver.solve[Expr, Cardinality](expr, 2000, small)

    assert(s(p("0 -> 0")).contains(Finite(1)))
    assert(s(p("1 -> 0")).contains(Finite(0)))
    assert(s(p("0 -> 1")).contains(Finite(1)))
    assert(s(p("∀ x. 3")).contains(Finite(3)))
    assert(s(p("∀ x. x")).contains(Finite(0)))
    assert(s(p("∀ x. x -> 0")).contains(Finite(0)))
    assert(s(p("∀ x. 1 -> x")).contains(Finite(0)))
    assert(s(p("∀ x. x -> x")).contains(Finite(1)))
    assert(s(p("∀ x. x * x -> x")).contains(Finite(2)))
    assert(s(p("∀ x. x * x -> x * x")).contains(Finite(4)))
    assert(s(p("∀ x. (x + x) -> x")).contains(Finite(1)))
    assert(s(p("∀ x. (x + x * x) -> x")).contains(Finite(2)))
    assert(s(p("∀ x. (1 + x) -> (1 + x)")).contains(Finite(2)))
    assert(s(p("∀ x. (1 + x * x) -> (1 + x)")).contains(Finite(3)))
    assert(s(p("∀ a. (0 -> a) -> a -> 0")).contains(Finite(0)))
    assert(s(p("∀ a. (a -> 0) -> 0")).contains(Finite(0)))
    assert(s(p("∀ a b. a * b -> a")).contains(Finite(1)))
    assert(s(p("∀ a b c. (a -> c) -> b -> a -> c")).contains(Finite(1)))
    assert(s(p("∀ a b c. (a -> b) -> (b -> c) -> a -> c")).contains(Finite(1)))
    assert(s(p("∀ a b. (a -> b) -> b -> a -> b")).contains(Finite(2)))
    assert(s(p("∀ a b c. (a -> b) -> b -> a -> c")).contains(Finite(0)))
    assert(s(p("∀ a b. (a -> 0) * b -> 2 * b")).contains(Finite(2)))
    assert(s(p("∀ a b. (a -> b) -> (1 + a) -> (1 + b)")).contains(Finite(2)))
    assert(s(p("∀ a. a -> (a -> a)")).contains(Finite(2)))
    assert(s(p("∀ a. a -> (a -> 0)")).contains(Finite(0)))
    assert(s(p("∀ a. (∀ b. a) -> a")).contains(Finite(1)))
    assert(s(p("∀ a. a -> ((a -> 0) + a)")).contains(Finite(1)))
    assert(s(p("∀ a. (a -> a) -> 0")).contains(Finite(0)))

//    assert(s(p("∀ a. (a -> a) -> a")).contains(Finite(0)))
//    assert(s(p("∀ a. a -> (a -> a) -> a")).contains(Infinite))

//    assert(s(p("∀ a. (a -> 0) -> a -> 0")).contains(Finite(1)))
//    assert(s(p("∀ a. ((a -> 0) * (a -> 0)) -> (a -> 0)")).contains(Finite(1)))
  }

  property("currently not working") {
    import Gens._
    import Expr._
    import Cardinality._

    val p = {
      val p = ExprAlg.parser[Expr](Expr.initial)
      s: String => p(s).either.right.get
    }

    def small : Expr => Result[Expr, Cardinality] = {
      case Fin(x) => Result.done(x)
      case e =>
        val r = (Expr.normRule.orId.bottomUp.fix andThen Expr.isoRule.andId.bottomUp).run(e).map { e1 =>
          if (!e1.isClosed(0)) sys.error(s"$e ~~> $e1")
          e1
        }
        Result.fork(r)
    }

    def s = (expr: Expr) => Solver.solve[Expr, Cardinality](expr, 2000, small)


    forAll(Gens.expr(0, 32), MinSuccessful(1000)) { e =>
      println(e)
      assert(s(e).isDefined)
    }
  }
}