autoCompilerPlugins := true

lazy val commonSettings = List(
  organization      := "com.alexknvl",
  version           := "0.1.0",
  scalaVersion      := "2.12.8",

  scalacOptions ++= List(
    "-deprecation",
    "-encoding", "UTF-8",
    "-explaintypes",
    "-Yrangepos",
    "-feature",
    "-Xfuture",
    "-Ypartial-unification",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-language:experimental.macros",
    "-unchecked",
    "-Yno-adapted-args",
    "-opt-warnings",
    "-Xlint:_,-type-parameter-shadow",
    "-Xsource:2.13",
    "-Ywarn-dead-code",
    "-Ywarn-extra-implicit",
    "-Ywarn-inaccessible",
    "-Ywarn-infer-any",
    "-Ywarn-nullary-override",
    "-Ywarn-nullary-unit",
    "-Ywarn-numeric-widen",
    "-Ywarn-unused:_,-imports",
    "-Ywarn-value-discard",
    "-opt:l:inline",
    "-opt-inline-from:<source>"),

  addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0-M4"),
  addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.4"),
  libraryDependencies += "com.lihaoyi" %% "acyclic" % "0.1.7" % "provided",
  addCompilerPlugin("com.lihaoyi" %% "acyclic" % "0.1.7"),
  scalacOptions += "-P:acyclic:force",
  addCompilerPlugin("org.scalamacros" % "paradise_2.12.8" % "2.1.0")
)

val attoVersion = "0.6.3"

lazy val hashing = (project in file("hashing"))
  .settings(name := "hashing")
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= List(
    "org.typelevel"  %% "cats-core" % "1.5.0"))

lazy val algebra = (project in file("algebra"))
  .settings(name := "algebra")
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= List(
    "org.typelevel"  %% "cats-core" % "1.5.0"))
  .settings(libraryDependencies += scalaOrganization.value % "scala-compiler" % scalaVersion.value)

lazy val typealg = (project in file("typealg"))
  .settings(name := "typealg")
  .settings(commonSettings: _*)
  .dependsOn(algebra, hashing)
  .settings(libraryDependencies ++= List(
    "org.typelevel"  %% "cats-core" % "1.5.0",
    "org.typelevel"  %% "kittens"   % "1.2.0",
    "org.typelevel"  %% "algebra"   % "1.0.0",
    "org.tpolecat"   %% "atto-core" % attoVersion,
    "org.scalatest"  %% "scalatest" % "3.0.5" % "test",
    "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"))

lazy val rest = (project in file("rest"))
  .enablePlugins(PackPlugin)
  .settings(name := "rest")
  .settings(commonSettings: _*)
  .dependsOn(typealg)
  .settings(libraryDependencies ++= List(
    "org.typelevel"  %% "cats-core"           % "1.5.0",
    "org.typelevel"  %% "kittens"             % "1.2.0",
    "org.typelevel"  %% "algebra"             % "1.0.0",
    "org.tpolecat"   %% "atto-core"           % attoVersion,
    "org.http4s"     %% "http4s-blaze-server" % "0.18.21",
    "org.http4s"     %% "http4s-circe"        % "0.18.21",
    "org.http4s"     %% "http4s-dsl"          % "0.18.21"))

lazy val root = Project(base = file("."), id = "typecounting")
  .aggregate(algebra, typealg)