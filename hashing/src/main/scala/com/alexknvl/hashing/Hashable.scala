package com.alexknvl.hashing
import cats.Order

trait Hashable[A] { A =>
  def hash[Z](z: Z, a: A)(implicit Z: Hash[Z]): Z

  def hash1[Z](a: A)(implicit Z: Hash[Z]): Z =
    hash(Z.empty, a)
}
object Hashable {
  def apply[A](implicit A: Hashable[A]): Hashable[A] = A

  def hash[Z: Hash, A: Hashable](z: Z, a: A): Z =
    Hashable[A].hash(z, a)

  def hash[Z: Hash, A: Hashable, B: Hashable](z0: Z, a: A, b: B): Z = {
    val z1 = Hashable[A].hash(z0, a)
    val z2 = Hashable[B].hash(z1, b)
    z2
  }

  def hash[Z: Hash, A: Hashable, B: Hashable, C: Hashable](z0: Z, a: A, b: B, c: C): Z = {
    val z1 = Hashable[A].hash(z0, a)
    val z2 = Hashable[B].hash(z1, b)
    val z3 = Hashable[C].hash(z2, c)
    z3
  }

  implicit val int: Hashable[Int] = new Hashable[Int] {
    def hash[Z](z: Z, a: Int)(implicit Z: Hash[Z]): Z =
      Z.int(z, a)
  }

  implicit val bigint: Hashable[BigInt] = new Hashable[BigInt] {
    def hash[Z](z: Z, a: BigInt)(implicit Z: Hash[Z]): Z =
      Z.bytes(z, a.toByteArray)
  }

  implicit def list[A](implicit A: Hashable[A]): Hashable[List[A]] =
    new Hashable[List[A]] {
      def hash[Z](z: Z, list: List[A])(implicit Z: Hash[Z]): Z =
        list.foldRight(Z.long(z, 0x8c6b486a6a044063L))((a, z) => A.hash(z, a))
    }

  implicit def set[A](implicit A: Hashable[A], ord: Order[A]): Hashable[Set[A]] =
    new Hashable[Set[A]] {
      def hash[Z](z: Z, set: Set[A])(implicit Z: Hash[Z]): Z = {
        val list = set.toList.sortWith(ord.lt)
        list.foldRight(Z.long(z, 0x9fcda00fb8f98458L))((a, z) => A.hash(z, a))
      }
    }
}