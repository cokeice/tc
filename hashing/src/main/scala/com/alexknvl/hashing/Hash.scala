package com.alexknvl.hashing

trait Hash[Z] {
  def empty: Z

  def bytes(z: Z, bytes: Array[Byte]): Z

  def byte(z: Z, x: Byte): Z =
    bytes(z, Array(x))

  def short(z: Z, x: Short): Z =
    bytes(z, Array((x >>> 8).toByte, (x & 0xFF).toByte))

  def int(z: Z, x: Int): Z =
    bytes(z, Array(
      (x >>> 24).toByte,
      ((x >>> 16) & 0xFF).toByte,
      ((x >>> 8) & 0xFF).toByte,
      (x & 0xFF).toByte))

  def long(z: Z, x: Long): Z = bytes(z, Array(
    ((x >>> 56) & 0xFF).toByte,
    ((x >>> 48) & 0xFF).toByte,
    ((x >>> 40) & 0xFF).toByte,
    ((x >>> 32) & 0xFF).toByte,
    ((x >>> 24) & 0xFF).toByte,
    ((x >>> 16) & 0xFF).toByte,
    ((x >>> 8) & 0xFF).toByte,
    (x & 0xFF).toByte))
}
