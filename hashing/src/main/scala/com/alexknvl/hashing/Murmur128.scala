package com.alexknvl.hashing

final case class Murmur128(lowPart: Long, highPart: Long)
object Murmur128 {
  implicit val hash: Hash[Murmur128] = new Hash[Murmur128] {
    override def empty = Murmur128(0L, 0L)
    override def bytes(z: Murmur128, bytes: Array[Byte]): Murmur128 = {
      val result = new Murmur3.LongPair()
      result.val1 = z.lowPart
      result.val2 = z.highPart
      Murmur3.murmurhash3_x64_128(bytes, 0, bytes.length, result)
      Murmur128(result.val1, result.val2)
    }
  }
}
 